import { Injectable } from '@angular/core';
import { Papa } from 'ngx-papaparse';

@Injectable({
  providedIn: 'root'
})
export class UserIssueListService {

  constructor(
    private papa: Papa
  ) { }

  parseForMatTable(csvData: string, headersDef) {
    const parsed = this.parseCsv(csvData);
    const tableRows = [];
    parsed.data.map((obj, index: number) => {
      const tableRow = {
        position: index + 1
      };
      for (const def of headersDef) {
        if (obj[def.csvHeader]) {
          tableRow[def.header] = obj[def.csvHeader];
        }
      }
      tableRows.push(tableRow);
    });
    return tableRows;
  }

  private parseCsv(csvData: string) {
    // could add string to date conversion
    return this.papa.parse(csvData, {
      header: true,
      dynamicTyping: true
    });
  }
}
