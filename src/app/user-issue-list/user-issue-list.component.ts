import { Component, OnInit, ViewChild } from '@angular/core';
import { UserIssueListService } from './user-issue-list.service';
import { CdkHeaderRowDef } from '@angular/cdk/table';
import { MatTableDataSource } from '@angular/material/table';
import { HeaderDef } from './shared/headers';
import { UserIssue } from './shared/userIssue.model';

@Component({
  selector: 'app-user-issue-list',
  templateUrl: './user-issue-list.component.html',
  styleUrls: ['./user-issue-list.component.scss']
})
export class UserIssueListComponent implements OnInit {
  @ViewChild('file', { static: false }) file;


  fileName: string;
  displayedColumns: string[];
  userIssueList: UserIssue[];
  dataSource = new MatTableDataSource();

  constructor(
    private userIssueListService: UserIssueListService
  ) {
    this.displayedColumns = HeaderDef.map(o => o.header);
  }

  ngOnInit() {
    this.dataSource.filterPredicate = (data: UserIssue, filter) =>
      (data.issueCount > Number(filter));
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded(event) {
    return new Promise((resolve, reject) => {
      if (event.files.length) {
        const file = event.files[0];
        this.fileName = file.name;
        const reader = new FileReader();
        // check for the validity of the csv file
        reader.readAsText(file);
        reader.onload = () => {
          // check for validity of content
          this.setTableData(reader.result as string);
          resolve();
        };
      } else {
        resolve();
      }
    });
  }

  setTableData(csvData: string) {
    this.dataSource.data = this.userIssueListService.parseForMatTable(csvData, HeaderDef);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim();
  }
}
