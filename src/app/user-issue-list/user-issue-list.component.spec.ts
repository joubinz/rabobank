import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserIssueListComponent} from './user-issue-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatLineModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserIssueListService } from './user-issue-list.service';
import { HeaderDef } from './shared/headers';
import { UserIssue } from './shared/userIssue.model';


const EXAMPLE_TABLE_DATA: UserIssue[] = [
  { position: 1, name: 'Theo', surname: 'Jansen', issueCount: 5, dob: '1978-01-02T00:00:00' },
  { position: 2, name: 'Fiona', surname: 'de Vries', issueCount: 7, dob: '1950-11-12T00:00:00' }
];

let userIssueListServiceStub: Partial<UserIssueListService>;
userIssueListServiceStub = {
  parseForMatTable: (csvData: any, headersDef: any) => [{}]
};

describe('UserIssueListComponent', () => {
  let component: UserIssueListComponent;
  let fixture: ComponentFixture<UserIssueListComponent>;

  let userIssueListService: UserIssueListService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        MatCardModule,
        MatLineModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        BrowserAnimationsModule
      ],
      declarations: [
        UserIssueListComponent
      ],
      providers: [
        { provide: UserIssueListService, useValue: userIssueListServiceStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserIssueListComponent);
    component = fixture.componentInstance;
    userIssueListService = TestBed.get(UserIssueListService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should pass the content of file when a file is selected', async () => {
    spyOn(userIssueListService, 'parseForMatTable');
    const MockBlob = new Blob(['TestFileContent'], { type: 'text/csv' });
    // tslint:disable no-string-literal
    MockBlob['name'] = 'example.csv';
    // tslint:enable no-string-literal
    const MockFile = MockBlob as File;
    const MockFiles: FileList = {
      0: MockFile,
      2: MockFile,
      length: 2,
      item: (index: number) => MockFile
    };
    await component.onFilesAdded({ files: MockFiles });
    expect(userIssueListService.parseForMatTable).toHaveBeenCalledWith('TestFileContent', HeaderDef);
  });

  it('should not pass the content of file when file is not selected', async () => {
    spyOn(userIssueListService, 'parseForMatTable');
    const MockBlob = new Blob(['TestFileContent'], { type: 'text/csv' });
    const MockFile = MockBlob as File;
    const MockFiles: FileList = {
      length: 0,
      item: (index: number) => MockFile
    };
    await component.onFilesAdded({ files: MockFiles });
    expect(userIssueListService.parseForMatTable).not.toHaveBeenCalled();
  });

  it('should populate the table in view correctly', () => {
    spyOn(userIssueListService, 'parseForMatTable').and.returnValue(EXAMPLE_TABLE_DATA);
    component.setTableData('');
    fixture.detectChanges();
    const tableRows = fixture.nativeElement.querySelectorAll('tr');
    // number of table rows plus one for the header
    expect(tableRows.length).toBe(3);
    // Header row more relevant if the heqader row is dynamically set
    const headerRow = tableRows[0];
    expect(headerRow.cells[0].innerHTML.trim()).toBe(HeaderDef[0].csvHeader);
    expect(headerRow.cells[1].innerHTML.trim()).toBe(HeaderDef[1].csvHeader);
    expect(headerRow.cells[2].innerHTML.trim()).toBe(HeaderDef[2].csvHeader);
    expect(headerRow.cells[3].innerHTML.trim()).toBe(HeaderDef[3].csvHeader);
    const row1 = tableRows[1];
    expect(row1.cells[0].innerHTML.trim()).toBe(EXAMPLE_TABLE_DATA[0].name);
    expect(row1.cells[1].innerHTML.trim()).toBe(EXAMPLE_TABLE_DATA[0].surname);
    expect(Number(row1.cells[2].innerHTML.trim())).toBe(EXAMPLE_TABLE_DATA[0].issueCount);
    expect(row1.cells[3].innerHTML.trim()).toBe(EXAMPLE_TABLE_DATA[0].dob);
    const row2 = tableRows[2];
    expect(row2.cells[0].innerHTML.trim()).toBe(EXAMPLE_TABLE_DATA[1].name);
    expect(row2.cells[1].innerHTML.trim()).toBe(EXAMPLE_TABLE_DATA[1].surname);
    expect(Number(row2.cells[2].innerHTML.trim())).toBe(EXAMPLE_TABLE_DATA[1].issueCount);
    expect(row2.cells[3].innerHTML.trim()).toBe(EXAMPLE_TABLE_DATA[1].dob);
  });

  it('should filter for minimal issue count', ( ) => {
    spyOn(userIssueListService, 'parseForMatTable').and.returnValue(EXAMPLE_TABLE_DATA);
    const filter = '6';
    component.setTableData('');
    component.applyFilter(filter);
    fixture.detectChanges();
    const tableRows = fixture.nativeElement.querySelectorAll('tr');
    // number of table rows plus one for the header
    expect(tableRows.length).toBe(2);
    const row1 = tableRows[1];
    expect(Number(row1.cells[2].innerHTML.trim())).toBeGreaterThan(Number(filter));
  });
});
