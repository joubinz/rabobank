import { TestBed } from '@angular/core/testing';

import { UserIssueListService } from './user-issue-list.service';
import { UserIssue } from './shared/userIssue.model';
import { HeaderDef } from './shared/headers';

const EXAMPLE_TABLE_DATA: UserIssue[] = [
  { position: 1, name: 'Theo', surname: 'Jansen', issueCount: 5, dob: '1978-01-02T00:00:00' },
  { position: 2, name: 'Fiona', surname: 'de Vries', issueCount: 7, dob: '1950-11-12T00:00:00' }
];

const EXAMPLE_FILE_DATA =
  `"First name","Sur name","Issue count","Date of birth"
"Theo","Jansen",5,"1978-01-02T00:00:00"
"Fiona","de Vries",7,"1950-11-12T00:00:00"`;

describe('UserIssueListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserIssueListService = TestBed.get(UserIssueListService);
    expect(service).toBeTruthy();
  });

  it('should parse the file data correctly', () => {
    const service: UserIssueListService = TestBed.get(UserIssueListService);
    const parsedForTable = service.parseForMatTable(EXAMPLE_FILE_DATA, HeaderDef);
    expect(parsedForTable).toEqual(EXAMPLE_TABLE_DATA);
  });
});
