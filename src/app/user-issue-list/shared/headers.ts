export const HeaderDef = [
    { header: 'name', csvHeader: 'First name' },
    { header: 'surname', csvHeader: 'Sur name' },
    { header: 'issueCount', csvHeader: 'Issue count' },
    { header: 'dob', csvHeader: 'Date of birth' }
];
