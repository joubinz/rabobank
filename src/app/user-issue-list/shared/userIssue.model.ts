export interface UserIssue {
    position: number;
    name: string;
    surname: string;
    issueCount: number;
    dob: string;
  }

